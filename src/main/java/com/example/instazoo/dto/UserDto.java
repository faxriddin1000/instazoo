package com.example.instazoo.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Data
public class UserDto {

  private Long id;
  @NotEmpty
  private String firstname;
  @NotEmpty
  private String lastname;

  private String bio;

}

