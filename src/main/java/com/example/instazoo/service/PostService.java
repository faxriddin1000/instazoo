package com.example.instazoo.service;


import com.example.instazoo.dto.PostDto;
import com.example.instazoo.entity.ImageModel;
import com.example.instazoo.entity.Post;
import com.example.instazoo.entity.User;
import com.example.instazoo.exception.PosNotFoundException;
import com.example.instazoo.repository.ImageModelRepository;
import com.example.instazoo.repository.PostRepository;
import com.example.instazoo.repository.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Service
public class PostService {

    public static final Logger LOG = LoggerFactory.getLogger(PostService.class);

    private PostRepository postRepository;
    private UserRepository userRepository;
    private ImageModelRepository imageModelRepository;


    public PostService(PostRepository postRepository, UserRepository userRepository, ImageModelRepository imageModelRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.imageModelRepository = imageModelRepository;
    }






    public Post createPost(PostDto postDto, Principal principal){
      User user = getUserNamPrincipal(principal);
      Post post  = new Post();

      post.setUser(user);
      post.setCaption(postDto.getCaption());
      post.setTitle(postDto.getTitle());
      post.setLocation(postDto.getLocation());
      post.setLikes(0);


      LOG.info("Saving Post for User: {} ", user.getEmail());
      return  postRepository.save(post);


    }


    public List<Post> getAllPosts(){
        return postRepository.findAllByOrderByCreatedDateDesc();
    }


    public List<Post> getAllPostForUser(Principal principal){
         User user = getUserNamPrincipal(principal);
         return postRepository.findAllByUserOrderByCreatedDateDesc(user);
    }


     public Post likesPost(Long postId, String username){
         Post post  = postRepository.findById(postId)
                 .orElseThrow(()-> new PosNotFoundException("Post cannot be not found"));

         Optional<String> userLiked = post.getLikedUsers()
                 .stream()
                 .filter(s -> s.equals(username)).findAny();

         if(userLiked.isPresent()){
            post.setLikes(post.getLikes() -1);
            post.getLikedUsers().remove(username);
         }else{
             post.setLikes(post.getLikes() + 1);
             post.getLikedUsers().add(username);
         }
         return postRepository.save(post);

     }


     public void deletePost(Long postId, Principal principal){
         Post post  =  getPostById(postId, principal);
         Optional<ImageModel>  imagemodel = imageModelRepository.findByPostId(post.getId());
         postRepository.delete(post);
         imagemodel.ifPresent(imageModelRepository::delete);
     }

    public Post getPostById(Long postId, Principal principal){
      User user = getUserNamPrincipal(principal);
      return postRepository.findPostByIdAndUser(postId, user)
              .orElseThrow( ()-> new PosNotFoundException("Post cannot be not found: {}"+ user.getEmail()));
    }


    private User getUserNamPrincipal(Principal principal){
        String username = principal.getName();
        return  userRepository.findUserByUsername(username)
                .orElseThrow(()-> new UsernameNotFoundException("User not found  "  +  username));


    }
}
