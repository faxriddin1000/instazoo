package com.example.instazoo.service;

import com.example.instazoo.dto.UserDto;
import com.example.instazoo.entity.User;
import com.example.instazoo.entity.enums.ERole;
import com.example.instazoo.exception.UserExistException;
import com.example.instazoo.playload.request.SignupRequest;
import com.example.instazoo.repository.UserRepository;
import com.example.instazoo.security.JWTTokenProvider;
import lombok.extern.java.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
public class UserService {
    public static final Logger LOG = LoggerFactory.getLogger(JWTTokenProvider.class);


    private UserRepository userRepository;
    private BCryptPasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }



    public User createUser(SignupRequest userIn){
        User user = new User();
        user.setEmail(userIn.getEmail());
        user.setName(userIn.getFirstname());
        user.setLastname(userIn.getLastname());
        user.setUsername(userIn.getUsername());
        user.setPassword(passwordEncoder.encode(userIn.getPassword()));
        user.getRole().add(ERole.ROLE_USER);

        try {
            LOG.info("Saving user {}" + userIn.getEmail());
            return userRepository.save(user);
        }catch (Exception ex){
            LOG.error("Registration Error. {}", ex.getMessage());
            throw new UserExistException("the user " + user.getUsername());

        }

    }


     public User updateUser(UserDto userDto, Principal principal){
         User user = getUserNamPrincipal(principal);
         user.setName(userDto.getFirstname());
         user.setLastname(userDto.getLastname());
         user.setBio(userDto.getBio());

         return userRepository.save(user);

     }


     public User getUserById(Long userId){
         User user = userRepository.findUserById(userId)
                 .orElseThrow(()-> new UsernameNotFoundException("User not found" + userId));
        return user;
     }

     public User getCurrencyUser(Principal principal){
         return getUserNamPrincipal(principal);
     }


     private User getUserNamPrincipal(Principal principal){
        String username = principal.getName();
        return  userRepository.findUserByUsername(username)
                .orElseThrow(()-> new UsernameNotFoundException("User not found  "  +  username));
     }







}
