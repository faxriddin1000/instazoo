package com.example.instazoo.service;


import com.example.instazoo.dto.CommentDto;
import com.example.instazoo.entity.Comment;
import com.example.instazoo.entity.Post;
import com.example.instazoo.entity.User;
import com.example.instazoo.exception.PosNotFoundException;
import com.example.instazoo.repository.CommentRepository;
import com.example.instazoo.repository.PostRepository;
import com.example.instazoo.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Service
public class CommentService {

    public static final Logger LOG = LoggerFactory.getLogger(CommentService.class);



    private CommentRepository commentRepository;
    private PostRepository postRepository;
    private UserRepository userRepository;


    public CommentService(CommentRepository commentRepository, PostRepository postRepository, UserRepository userRepository) {
        this.commentRepository = commentRepository;
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }





    public Comment saveComment(Long postId, CommentDto commentDto, Principal principal){
      User user = getUserNamPrincipal(principal);
      Post post = postRepository.findById(postId)
              .orElseThrow(()-> new PosNotFoundException("Post cannot be not found for username: " + user.getEmail()));


      Comment comment = new Comment();
      comment.setPost(post);
      comment.setUserId(user.getId());
      comment.setMessage(commentDto.getMessage());
      comment.setUsername(user.getUsername());

      LOG.info("Saving comment for Post: {}", post.getId());
      return commentRepository.save(comment);

    }


    public List<Comment> getAllCommentsForPost(Long postId){
       Post post  = postRepository.findById(postId)
               .orElseThrow(() -> new PosNotFoundException("Post cannot be not found"));


       List<Comment> comments = commentRepository.findAllByPost(post);
       return comments;
    }



    public void deleteComment(Long commentId){
        Optional<Comment> comment = commentRepository.findById(commentId);
        comment.ifPresent(commentRepository::delete);


    }

    private User getUserNamPrincipal(Principal principal){
        String username = principal.getName();
        return  userRepository.findUserByUsername(username)
                .orElseThrow(()-> new UsernameNotFoundException("User not found  "  +  username));


    }

















}
