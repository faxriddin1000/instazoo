package com.example.instazoo.service;
import com.example.instazoo.entity.User;
import com.example.instazoo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomUserdetailsService implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public CustomUserdetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email){
        User user = userRepository.findUserByEmail(email)
                 .orElseThrow(()-> new UsernameNotFoundException("User not found  " + email));
        return build(user);
    }


    public User loadUserById(Long userId){
        return userRepository.findUserById(userId).orElse(null);
    }


    public static User build(User user){
            List<GrantedAuthority> authorities = user.getRole().stream()
                     .map(role -> new SimpleGrantedAuthority(role.name()))
                     .collect(Collectors.toList());

            return new User(
                    user.getId(),
                    user.getUsername(),
                    user.getEmail(),
                    user.getPassword(),
                    authorities);
    }











}
