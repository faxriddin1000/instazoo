package com.example.instazoo.web;


import com.example.instazoo.dto.PostDto;
import com.example.instazoo.entity.Post;
import com.example.instazoo.facade.PostFacade;
import com.example.instazoo.playload.response.MessageResponse;
import com.example.instazoo.service.PostService;
import com.example.instazoo.validations.ResponseErrorValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/post")
@CrossOrigin
public class PostController {

     @Autowired
     public PostService postService;
     @Autowired
     public PostFacade postFacade;
     @Autowired
     public ResponseErrorValidation responseErrorValidation;


     @PostMapping("/create")
     public ResponseEntity<Object> createPost(@Valid @RequestBody PostDto postDto, Principal principal, BindingResult bindingResult){
          ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
          if(!ObjectUtils.isEmpty(errors)){ return  errors;}
          Post post = postService.createPost(postDto, principal);
          PostDto postCerate = postFacade.postTopostDto(post);
         return new ResponseEntity<>(postCerate, HttpStatus.OK);

     }



      @GetMapping("/all")
      public ResponseEntity<List<PostDto>> getAllPost(){
         List<PostDto> allPost = postService.getAllPosts()
                  .stream()
                  .map(postFacade::postTopostDto)
                  .collect(Collectors.toList());

         return new ResponseEntity<>(allPost, HttpStatus.OK);
      }


      @GetMapping("/user/posts")
      public ResponseEntity<List<PostDto>> getAllPostsForUser(Principal principal){
          List<PostDto> allPostForUser = postService.getAllPostForUser(principal)
                    .stream()
                    .map(postFacade::postTopostDto)
                    .collect(Collectors.toList());

          return new ResponseEntity<>(allPostForUser, HttpStatus.OK);


      }


      @PostMapping("/{postId}/{username}/like")
      public ResponseEntity<PostDto> likesPost(@PathVariable("postId") String postId, @PathVariable("username") String username){
           Post post = postService.likesPost(Long.parseLong(postId), username);
           PostDto postDto = postFacade.postTopostDto(post);
          return new ResponseEntity<>(postDto, HttpStatus.OK);
      }




      @PostMapping("/delete/{postId}")
      public ResponseEntity<MessageResponse> deletePost(@PathVariable("postId") String postId, Principal principal){
         postService.deletePost(Long.parseLong(postId), principal);
        return new ResponseEntity<>(new MessageResponse("Post was deleted"), HttpStatus.OK);
      }




}
