package com.example.instazoo.web;


import com.example.instazoo.dto.UserDto;
import com.example.instazoo.entity.User;
import com.example.instazoo.facade.UserFacade;
import com.example.instazoo.service.UserService;
import com.example.instazoo.validations.ResponseErrorValidation;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping("/api/user")
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserFacade userFacade;

    @Autowired
    private ResponseErrorValidation responseErrorValidation;



    @GetMapping("/")
    public ResponseEntity<UserDto> getCurrentUser(Principal principal){
         User user = userService.getCurrencyUser(principal);
         UserDto userDto = userFacade.userToUserDto(user);
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }



    @GetMapping("/{userId}")
    public ResponseEntity<UserDto> getUserProfile(@PathVariable("userId") String userId){
         User user = userService.getUserById(Long.parseLong(userId));
         UserDto userDto = userFacade.userToUserDto(user);
       return new ResponseEntity<>(userDto, HttpStatus.OK);


    }



    @PostMapping("/update")
    public ResponseEntity<Object> update(@Valid @RequestBody UserDto userDto, BindingResult bindingResult, Principal principal){
         ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
         if(!ObjectUtils.isEmpty(errors)){
              return errors;
         }

          User user = userService.updateUser(userDto, principal);
          UserDto userUpdated = userFacade.userToUserDto(user);
        return  new ResponseEntity<>(userUpdated, HttpStatus.OK);
   }




}
