package com.example.instazoo.web;


import com.example.instazoo.dto.CommentDto;
import com.example.instazoo.entity.Comment;
import com.example.instazoo.facade.CommentFacade;
import com.example.instazoo.facade.PostFacade;
import com.example.instazoo.playload.response.MessageResponse;
import com.example.instazoo.service.CommentService;
import com.example.instazoo.service.PostService;
import com.example.instazoo.validations.ResponseErrorValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/comment")
@CrossOrigin
public class CommentController {


     @Autowired
     private CommentService commentService;
     @Autowired
     private CommentFacade commentFacade;
     @Autowired
     private ResponseErrorValidation responseErrorValidation;




     @PostMapping("/{postId}/create")
    public ResponseEntity<Object> createComment(@Valid @RequestBody CommentDto commentDto, @PathVariable("postId") String postId, Principal principal, BindingResult bindingResult){
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if(!ObjectUtils.isEmpty(errors)){return  errors;}
        Comment comment = commentService.saveComment(Long.parseLong(postId), commentDto, principal);
        CommentDto createComment = commentFacade.commentToCommentDto(comment);
       return new ResponseEntity<>(createComment, HttpStatus.OK);
    }


    @GetMapping("/{postId}")
    public ResponseEntity<List<CommentDto>> getAllComment(@PathVariable("postId") String postId){
         List<CommentDto> comment = commentService.getAllCommentsForPost(Long.parseLong(postId))
                  .stream()
                  .map(commentFacade:: commentToCommentDto)
                  .collect(Collectors.toList());

         return new ResponseEntity<>(comment, HttpStatus.OK);
    }


    @PostMapping("/{postId}/delete")
    public ResponseEntity<MessageResponse> deleteComment(@PathVariable("postId") String postId){
        commentService.deleteComment(Long.parseLong(postId));
        return new ResponseEntity<>(new MessageResponse("Comment is deleted"), HttpStatus.OK);

    }

}
