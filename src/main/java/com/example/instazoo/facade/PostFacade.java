package com.example.instazoo.facade;

import com.example.instazoo.dto.PostDto;
import com.example.instazoo.entity.Post;
import org.springframework.stereotype.Component;

@Component
public class PostFacade {

    public PostDto postTopostDto(Post post){
         PostDto postDto = new PostDto();
         postDto.setUsername(post.getUser().getUsername());
         postDto.setTitle(post.getTitle());
         postDto.setCaption(post.getCaption());
         postDto.setId(post.getId());
         postDto.setUsersLiked(post.getLikedUsers());
         postDto.setLocation(post.getLocation());
         return postDto;
 }

}
