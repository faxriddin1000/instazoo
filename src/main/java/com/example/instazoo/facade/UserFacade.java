package com.example.instazoo.facade;

import com.example.instazoo.dto.UserDto;
import com.example.instazoo.entity.User;
import org.springframework.stereotype.Component;

@Component
public class UserFacade {

  public UserDto userToUserDto(User user){
      UserDto userDto = new UserDto();
      userDto.setId(user.getId());
      userDto.setFirstname(user.getUsername());
      userDto.setLastname(user.getLastname());
      userDto.setBio(user.getBio());
      return userDto;
  }

}

