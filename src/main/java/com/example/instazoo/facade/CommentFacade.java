package com.example.instazoo.facade;


import com.example.instazoo.dto.CommentDto;
import com.example.instazoo.entity.Comment;
import org.springframework.stereotype.Component;

@Component
public class CommentFacade {


   public CommentDto commentToCommentDto(Comment comment){
       CommentDto commentDto = new CommentDto();
       commentDto.setId(comment.getId());
       commentDto.setUsername(comment.getUsername());
       commentDto.setMessage(comment.getMessage());
       return  commentDto;
 }

}
