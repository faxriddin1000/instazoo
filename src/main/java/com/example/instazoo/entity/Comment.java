package com.example.instazoo.entity;

import lombok.Data;
import lombok.With;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ManyToOne(fetch = FetchType.EAGER)
    private Post post;
    @Column(nullable = false)
    private String username;
    @Column(nullable = false)
    private Long userId;
    @Column(columnDefinition = "text")
    private String  message;
    @Column(updatable = false)
    private LocalDateTime  createdDate;


    @PrePersist
    protected void oncreate(){
         this.createdDate = LocalDateTime.now();
    }


}
