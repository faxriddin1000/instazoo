package com.example.instazoo.playload.request;


import com.example.instazoo.annotations.ValidEmail;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class SignupRequest {
    @Email(message = "it should have email format")
    @NotBlank(message = "User email is required")

    private String email;
    @NotBlank(message = "Firstname is required")
    private String firstname;
    @NotBlank(message = "LastName is required")
    private String lastname;
    @NotBlank(message = "UserName is required")
    private String username;
    @NotBlank(message = "Password is required")
    @Size(min = 6)
    private String password;
    private String confirmPassword;


}
