package com.example.instazoo.playload.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class LoginRequest {
     @NotEmpty(message = "Username cannot be is empty")
     private String  username;

    @NotEmpty(message = "Password cannot be is empty")
     private String password;

}
